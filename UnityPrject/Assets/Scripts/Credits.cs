using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    [SerializeField]
    private Button back;

    private void Awake()
    {
        back.onClick.AddListener(OnBack);
    }

    private void OnBack()
    {
        LevelChanger.Instance.MainMenuScene();
    }
}