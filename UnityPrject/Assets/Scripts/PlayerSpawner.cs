using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    private Player player;

    private void Awake()
    {
        Utils.SetObject(out player);
    }

    private void Start()
    {
        player.transform.position = transform.position;
        player.transform.rotation = transform.rotation;
    }
}