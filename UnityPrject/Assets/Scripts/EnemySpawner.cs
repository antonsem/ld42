using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;

    private GameObject[] enemies;

    private void Awake()
    {
        Instantiate(enemy, transform.position, transform.rotation);
        Events.Instance.enemyHit.AddListener(CreateEnemy);
    }

    private void CreateEnemy()
    {
        if (LevelGenerator.Instance.data.settings.maxEnemyCount >= FindObjectsOfType<Enemy>().Length)
            Instantiate(enemy, transform.position, transform.rotation);
    }
}