using System;
using System.IO;
using UnityEngine;

[Serializable]
public class LevelSettings
{
    public int pixelScale = 2;
    public int unBreakableWallRef = 0;
    public int padRef = 1;
    public int breakableWallRef = 2;
    public int itemRef = 3;
    public int enemySpawnerRef = 4;
    public int playerSpawnerRef = 5;
    public int itemCount = 4;
    public int maxEnemyCount = 3;
}

[Serializable]
public class Colors
{
    public Color pad = Color.black;
    public Color breakableWall = Color.black;
    public Color unBreakableWall = Color.black;
    public Color item = Color.black;
    public Color enemySpawner = Color.black;
    public Color playerSpawner = Color.black;

    public Colors(LevelSettings settings, Texture2D reference)
    {
        pad = reference.GetPixel(settings.padRef, 0);
        breakableWall = reference.GetPixel(settings.breakableWallRef, 0);
        unBreakableWall = reference.GetPixel(settings.unBreakableWallRef, 0);
        item = reference.GetPixel(settings.itemRef, 0);
        enemySpawner = reference.GetPixel(settings.enemySpawnerRef, 0);
        playerSpawner = reference.GetPixel(settings.playerSpawnerRef, 0);
    }
}

[CreateAssetMenu(fileName = "LevelData", menuName = "LevelData")]
public class LevelData : ScriptableObject
{
    [SerializeField]
    private TextAsset data;
    [SerializeField]
    public Colors colors;
    [SerializeField]
    private Texture2D levelTex;
    [SerializeField]
    private Texture2D referenceTex;
    [SerializeField]
    private PrefabHolder prefabs;


    [SerializeField]
    public LevelSettings settings;
    [SerializeField]
    public int width = 64;
    [SerializeField]
    public int height = 64;

    public void ReadSettings()
    {
        settings = JsonUtility.FromJson(data.text, typeof(LevelSettings)) as LevelSettings;
        colors = new Colors(settings, referenceTex);
        width = levelTex.width;
        height = levelTex.height;
    }

    public GameObject GetPrefab(int x, int y)
    {
        if (x > levelTex.width || y > levelTex.height || x < 0 || y < 0)
        {
            Debug.LogError("Index out of range! " + x + ", " + y);
            return null;
        }

        Color c = levelTex.GetPixel(x, y);
        if (c == colors.unBreakableWall)
            return prefabs.unBreakableWallPrefab;
        else if (c == colors.breakableWall)
            return prefabs.breakableWallPrefab;
        else if (c == colors.item)
            return prefabs.itemPrefab;
        else if (c == colors.pad)
            return prefabs.padPrefab;
        else if (c == colors.enemySpawner)
            return prefabs.enemySpawnerPrefab;
        else if (c == colors.playerSpawner)
            return prefabs.playerSpawnerPrefab;

        return null;
    }

    public GameObject GetGroundPrefab()
    {
        return prefabs.groundPrefab;
    }

    public GameObject GetBreakablePrefab()
    {
        return prefabs.breakableWallPrefab;
    }

    public GameObject GetItemPrefab()
    {
        return prefabs.itemPrefab;
    }

    private void CreateJson()
    {
        if (settings == null)
            settings = new LevelSettings();

        string str = JsonUtility.ToJson(settings, true);
        using (StreamWriter sw = new StreamWriter(Application.dataPath + "/settings.json"))
            sw.Write(str);
    }

    [Space(25), Header("Inspector Buttons"), SerializeField, InspectorButton("ReadSettings")]
    private string readSettings;
    [SerializeField, InspectorButton("CreateJson")]
    private string createJson;
}