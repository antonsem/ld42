using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using System;

public class InGameUI : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup fade;
    [SerializeField]
    private TextMeshProUGUI cellsText;
    [SerializeField]
    private TextMeshProUGUI sentinelsText;
    [SerializeField]
    private TextMeshProUGUI spaceText;
    [SerializeField]
    private TextMeshProUGUI healthText;
    [SerializeField]
    private TextMeshProUGUI message;
    [SerializeField]
    private Slider health;

    [Space(10), Header("Activations"), SerializeField]
    private GameObject radar;
    [SerializeField]
    private GameObject healthObj;
    [SerializeField]
    private GameObject cellsObj;
    [SerializeField]
    private GameObject sentinelsObj;
    [SerializeField]
    private GameObject spaceObj;
    [SerializeField]
    private GameObject pause;

    private Player player;
    private LevelChanger levelChanger;
    private Events events;

    private float messageTime = 0;

    private void Awake()
    {
        Utils.SetObject(out player, true);
        levelChanger = LevelChanger.Instance;

        events = Events.Instance;

        events.itemPickedUp.AddListener(UpdateCells);
        events.enemyIsBorn.AddListener(UpdateSentinels);
        events.tileCreated.AddListener(UpdateSpace);
        events.playerHit.AddListener(UpdateHealth);
        events.levelPassed.AddListener(() => FadeOut(true));
        events.playerDied.AddListener(() => FadeOut(false));
        events.sendMessage.AddListener(UpdateMessage);
        events.activateCellsCount.AddListener(ActivateCells);
        events.activateHealth.AddListener(ActivateHealth);
        events.activateRadar.AddListener(ActivateRadar);
        events.activateSentinels.AddListener(ActivateSentinels);
        events.activateSpaceCount.AddListener(ActivateSpaceCount);

        StartCoroutine(FadeFromTo(1, 0));
    }

    private void Start()
    {
        UpdateCells();
        UpdateHealth();
        UpdateSentinels();
    }

    private void FadeOut(bool nextScene)
    {
        if (nextScene)
            StartCoroutine(FadeFromTo(0, 1, levelChanger.ProceedToNextScene));
        else
            StartCoroutine(FadeFromTo(0, 1, levelChanger.GameOverScene));
    }

    private IEnumerator FadeFromTo(float from, float to, Action callback = null, float speed = 2)
    {
        fade.alpha = from;

        while (Mathf.Abs(fade.alpha - to) > 0.05f)
        {
            fade.alpha = Mathf.Lerp(fade.alpha, to, Time.deltaTime * speed);
            yield return null;
        }

        fade.alpha = to;

        if (callback != null)
            callback.Invoke();
    }

    private void ActivateCells(bool state)
    {
        cellsObj.SetActive(state);
        UpdateCells();
    }

    private void ActivateHealth(bool state)
    {
        healthObj.SetActive(state);
        UpdateHealth();
    }

    private void ActivateSentinels(bool state)
    {
        sentinelsObj.SetActive(state);
        UpdateSentinels();
    }

    private void ActivateSpaceCount(bool state)
    {
        spaceObj.SetActive(state);
        UpdateSpace();
    }

    private void ActivateRadar(bool state)
    {
        radar.SetActive(state);
    }

    private void UpdateCells()
    {
        cellsText.text = Inventory.Instance.Items.Count.ToString() + " / " + LevelGenerator.Instance.data.settings.itemCount.ToString();
    }

    private void UpdateSentinels()
    {
        sentinelsText.text = FindObjectsOfType<Enemy>().Length.ToString();
    }

    private void UpdateSpace()
    {
        spaceText.text = LevelGenerator.Instance.GetAcceptablePositions().Count.ToString();
    }

    private void UpdateHealth()
    {
        health.value = player.health / player.fullHealth;
        healthText.text = "% " + (health.value * 100).ToString();
    }

    private void UpdateMessage(string msg, float msgTime)
    {
        message.text = msg;
        messageTime = msgTime;
    }

    private void Update()
    {
        if (messageTime <= 0)
            UpdateMessage("", 0);
        else
            messageTime -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Escape))
            pause.SetActive(true);
    }
}