using UnityEngine;

public class CellTutorial : Activator
{
    public GameObject wall;
    public GameObject cell;
    [TextArea(20, 50)]
    public string message;
    public float messageTime = 10;
    public AudioClip clip;

    protected override void Activate()
    {
        wall.SetActive(false);
        cell.SetActive(true);
        Events.Instance.sendMessage.Invoke(message, messageTime);
        gameObject.SetActive(false);
        AudioSource.PlayClipAtPoint(clip, transform.position, 0.5f);
    }
}