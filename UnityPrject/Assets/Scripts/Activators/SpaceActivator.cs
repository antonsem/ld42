using UnityEngine;

public class SpaceActivator : Activator
{
    protected override void Activate()
    {
        Events.Instance.activateSpaceCount.Invoke(true);
    }
}