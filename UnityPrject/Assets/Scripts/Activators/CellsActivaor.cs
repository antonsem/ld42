using UnityEngine;

public class CellsActivaor : Activator
{
    protected override void Activate()
    {
        Events.Instance.activateCellsCount.Invoke(true);
    }
}