using UnityEngine;

public class SentinelsActivator : Activator
{
    protected override void Activate()
    {
        Events.Instance.activateSentinels.Invoke(true);
    }
}