public class GunActivator : Activator
{
    protected override void Activate()
    {
        Events.Instance.activateGun.Invoke(true);
    }
}