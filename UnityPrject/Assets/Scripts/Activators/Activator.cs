using System;
using UnityEngine;

public class Activator : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Player p;
        if(other.transform.SetComponent(out p))
            Activate();
    }

    protected virtual void Activate()
    {

    }
}