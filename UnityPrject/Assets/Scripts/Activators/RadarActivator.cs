using UnityEngine;

public class RadarActivator : Activator
{
    protected override void Activate()
    {
        Events.Instance.activateRadar.Invoke(true);
    }
}