using UnityEngine;

public class GunTutorial : Activator
{
    public GameObject gunActivator;
    [TextArea(20, 50)]
    public string message;
    public float messageTime = 10;
    [SerializeField]
    private AudioClip clip;

    protected override void Activate()
    {
        AudioSource.PlayClipAtPoint(clip, transform.position, 0.5f);
        gunActivator.SetActive(true);
        Events.Instance.sendMessage.Invoke(message, messageTime);
        gameObject.SetActive(false);
    }
}