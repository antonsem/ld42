using UnityEngine;

public class HealthActivator : Activator
{
    protected override void Activate()
    {
        Events.Instance.activateHealth.Invoke(true);
    }
}