using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PausePanel : MonoBehaviour
{
    [SerializeField]
    private GameObject inGame;
    private FirstPersonController controller;

    private void OnEnable()
    {
        if (controller || Utils.SetObject(out controller))
            controller.enabled = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        inGame.SetActive(true);
    }

    private void OnDisable()
    {
        if (controller || Utils.SetObject(out controller))
            controller.enabled = true;

        inGame.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
            LevelChanger.Instance.MainMenuScene();
        else if (Input.GetKeyDown(KeyCode.N))
        {
            inGame.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}