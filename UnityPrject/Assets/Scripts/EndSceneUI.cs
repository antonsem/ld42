using UnityEngine;
using UnityEngine.UI;

public class EndSceneUI : MonoBehaviour
{
    [SerializeField]
    private Button ok;

    private void Awake()
    {
        ok.onClick.AddListener(OnOk);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnOk()
    {
        LevelChanger.Instance.MainMenuScene();
    }
}