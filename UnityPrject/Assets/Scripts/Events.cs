using UnityEngine;
using UnityEngine.Events;

public class TileCreated : UnityEvent
{ }

public class TileDestroyed : UnityEvent
{ }

public class EnemyHit : UnityEvent
{ }

public class EnemyIsBorn : UnityEvent
{ }

public class ItemPickedUp : UnityEvent
{ }

public class PlayerHit : UnityEvent
{ }

public class LevelPassed : UnityEvent
{ }

public class PlayerDied : UnityEvent
{ }

public class SendMessage : UnityEvent<string, float>
{ }

public class ActivateHealth : UnityEvent<bool>
{ }

public class ActivateSentinelsCount : UnityEvent<bool>
{ }

public class ActivateCellsCount : UnityEvent<bool>
{ }

public class ActivateSpaceCount : UnityEvent<bool>
{ }

public class ActivateGun : UnityEvent<bool>
{ }

public class ActivateRadar : UnityEvent<bool>
{ }

public class Events : Singleton<Events>
{
    public TileCreated tileCreated = new TileCreated();
    public TileDestroyed tileDestroyed = new TileDestroyed();
    public EnemyHit enemyHit = new EnemyHit();
    public EnemyIsBorn enemyIsBorn = new EnemyIsBorn();
    public ItemPickedUp itemPickedUp = new ItemPickedUp();
    public PlayerHit playerHit = new PlayerHit();
    public LevelPassed levelPassed = new LevelPassed();
    public PlayerDied playerDied = new PlayerDied();
    public SendMessage sendMessage = new SendMessage();
    public ActivateHealth activateHealth = new ActivateHealth();
    public ActivateSentinelsCount activateSentinels = new ActivateSentinelsCount();
    public ActivateSpaceCount activateSpaceCount = new ActivateSpaceCount();
    public ActivateCellsCount activateCellsCount = new ActivateCellsCount();
    public ActivateGun activateGun = new ActivateGun();
    public ActivateRadar activateRadar = new ActivateRadar();
}