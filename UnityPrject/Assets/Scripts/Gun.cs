using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Gun : MonoBehaviour
{
    [SerializeField]
    protected GameObject bullet;
    [SerializeField]
    private Transform nozzle;
    [SerializeField]
    private AudioClip clip;
    private AudioSource source;

    public float cooldownTime = 1;
    private float timer = 0;

    private void Start()
    {
        transform.SetComponent(out source);

    }

    public virtual void Shoot()
    {
        if (timer > 0)
            return;
        timer = cooldownTime;

        Vector3 rotation = transform.rotation.eulerAngles;
        rotation.x = rotation.z = 0;
        Instantiate(bullet, nozzle.transform.position, Quaternion.Euler(rotation));
        source.pitch = Random.Range(0.9f, 1.1f);
        source.PlayOneShot(clip);
    }

    protected virtual void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
    }
}