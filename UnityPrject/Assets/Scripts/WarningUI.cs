using System.Collections;
using UnityEngine;

public class WarningUI : MonoBehaviour
{
    public float waitTime = 3;
    private bool active = false;
    public GameObject anyKeyPrompt;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(waitTime);
        active = true;
        anyKeyPrompt.SetActive(true);
    }

    private void Update()
    {
        if (active && Input.anyKeyDown)
            LevelChanger.Instance.MainMenuScene();
    }
}