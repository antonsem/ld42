using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelGenerator : Singleton<LevelGenerator>
{
    [SerializeField]
    private int currentLevelIndex = -1;
    [SerializeField]
    private bool randomizeLoot = true;
    public LevelData data;
    [SerializeField]
    private GameObject radarCam;
    [SerializeField, HideInInspector]
    private GameObject ground;
    [SerializeField, HideInInspector]
    private Transform tileRoot;
    [SerializeField, HideInInspector]
    private GameObject[] tiles;

    [SerializeField]
    private NavMeshSurface surface;

    private void Awake()
    {
        data.ReadSettings();
        if (randomizeLoot)
            RandomizeLoot();
    }

    private void Start()
    {
        if (currentLevelIndex != -1)
            LevelChanger.Instance.currentLevel = currentLevelIndex;

        SetRadar();
    }

    private void ClearLevel()
    {
        if (ground)
            DestroyImmediate(ground);
        if (tileRoot)
            DestroyImmediate(tileRoot.gameObject);
        else
            for (int i = 0; i < tiles.Length; i++)
                DestroyImmediate(tiles[i]);
    }

    private void CreateLevel()
    {
        data.ReadSettings();
        tiles = new GameObject[data.width * data.height];
        tileRoot = new GameObject("TileRoot").transform;

        Color texColor = Color.black;
        GameObject temp;
        for (int j = 0; j < data.height; j++)
        {
            for (int i = 0; i < data.width; i++)
            {
                temp = data.GetPrefab(i, j);
                if (temp)
                    tiles[i + data.width * j] = Instantiate(temp, new Vector3(i * data.settings.pixelScale, 1, j * data.settings.pixelScale), Quaternion.identity, tileRoot);
            }
        }

        SetPad();

        temp = null;
        ground = Instantiate(data.GetGroundPrefab());
        ground.transform.localScale = new Vector3(data.width * data.settings.pixelScale, 0.1f, data.height * data.settings.pixelScale);
        ground.transform.position = new Vector3(data.width * data.settings.pixelScale / 2 - 1, -0.05f, data.height * data.settings.pixelScale / 2 - 1);

        if (surface || Utils.SetObject(out surface))
            surface.BuildNavMesh();
        else
            Debug.LogWarning("Navmesh is not recreated!");

        SetRadar();

        Debug.Log("New level generated");
    }

    private void SetRadar()
    {
        radarCam.transform.position = new Vector3(data.width - 1, 20, data.height - 1);
        radarCam.transform.rotation = Quaternion.Euler(90, 0, 0);
    }

    public List<Vector2Int> GetAcceptablePositions()
    {
        List<Vector2Int> retVal = new List<Vector2Int>();
        for (int i = 0; i < tiles.Length; i++)
        {
            if (!tiles[i])
                retVal.Add(new Vector2Int(i % data.width, i / data.width));
        }

        return retVal;
    }

    public void CreateTile(Vector2Int pos, GameObject obj = null)
    {
        if (tiles[pos.x + data.width * pos.y])
        {
            Debug.LogError("Cannot create at this pos!");
            return;
        }

        if (!obj)
            obj = data.GetBreakablePrefab();
        tiles[pos.x + data.width * pos.y] = Instantiate(obj, new Vector3(pos.x * data.settings.pixelScale, 1, pos.y * data.settings.pixelScale), Quaternion.identity, tileRoot);
    }

    public void DestroyTile(GameObject obj, GameObject replacement = null)
    {
        bool destroyed = false;
        for (int i = 0; i < tiles.Length; i++)
        {
            if (tiles[i] == obj)
            {
                Destroy(obj);
                tiles[i] = replacement;
                destroyed = true;
                break;
            }
        }
        if (!destroyed)
            Debug.LogWarning("Tile " + obj.name + " is not registered!");
    }

    private void SetPad()
    {
        Item[] items = FindObjectsOfType<Item>();
        if (data.settings.itemCount < items.Length)
        {
            Debug.LogWarning("Preset items count is more then stated in json!");
            data.settings.itemCount = items.Length;
        }
        Pad p;
        if (Utils.SetObject(out p, true))
            p.requiredItems = data.settings.itemCount;
    }

    public void RandomizeLoot()
    {
        Item[] items = FindObjectsOfType<Item>();
        if (data.settings.itemCount > items.Length)
        {
            Tile[] brekableTiles = FindObjectsOfType<Tile>();
            new System.Random().Shuffle(brekableTiles);
            for (int i = 0; i < data.settings.itemCount - items.Length; i++)
            {
                bool done = false;
                for (int j = 0; j < brekableTiles.Length; j++)
                {
                    if (!brekableTiles[j].loot)
                    {
                        brekableTiles[j].loot = data.GetItemPrefab();
                        done = true;
                        break;
                    }
                }
                if (!done)
                {
                    Debug.LogError("Couldn't find enough breakable tiles to fit all of the items!");
                    break;
                }
            }
        }
    }

    private void RecreateLevel()
    {
        ClearLevel();
        CreateLevel();
    }

    [Space(25), Header("Inspector Buttons"), SerializeField, InspectorButton("RecreateLevel")]
    private string recreateLevel;
    [SerializeField, InspectorButton("CreateLevel")]
    private string createLevel;
    [SerializeField, InspectorButton("ClearLevel")]
    private string clearLevel;

}