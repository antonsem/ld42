using UnityEngine;

[CreateAssetMenu(fileName = "PrefabHolder", menuName = "Prefab Holder")]
public class PrefabHolder : ScriptableObject
{
    public GameObject groundPrefab;
    public GameObject unBreakableWallPrefab;
    public GameObject breakableWallPrefab;
    public GameObject itemPrefab;
    public GameObject padPrefab;
    public GameObject enemySpawnerPrefab;
    public GameObject playerSpawnerPrefab;
}