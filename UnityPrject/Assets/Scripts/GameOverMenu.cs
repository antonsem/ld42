using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField]
    private Button tryAgain;
    [SerializeField]
    private Button cancel;

    private LevelChanger levelChanger;

    private void Awake()
    {
        tryAgain.onClick.AddListener(OnTryAgain);
        cancel.onClick.AddListener(OnCancel);
        levelChanger = LevelChanger.Instance;
    }

    private void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnTryAgain()
    {
        levelChanger.ReloadCurrentLevel();
    }

    private void OnCancel()
    {
        levelChanger.MainMenuScene();
    }
}