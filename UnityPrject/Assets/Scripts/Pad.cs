using UnityEngine;

public class Pad : MonoBehaviour
{
    [SerializeField]
    public int requiredItems = 4;
    [SerializeField]
    private GameObject particles;
    [SerializeField]
    private AudioSource source;

    private bool active = false;

    private void Start()
    {
        CheckProgress();
        Events.Instance.itemPickedUp.AddListener(CheckProgress);
    }

    private void OnEnable()
    {
        CheckProgress();
    }

    private void CheckProgress()
    {
        int count = 0;
        for (int i = 0; i < Inventory.Instance.Items.Count; i++)
        {
            if (Inventory.Instance.Items[i] is Item)
                count++;
        }

        if (count >= requiredItems)
            active = true;
        particles.SetActive(active);
        source.enabled = active;
    }

    private void OnTriggerEnter(Collider other)
    {
        Player p;
        if (other.transform.SetComponent(out p))
        {
            if (active)
                Events.Instance.levelPassed.Invoke();
        }
    }
}