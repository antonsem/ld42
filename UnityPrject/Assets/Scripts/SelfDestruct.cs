using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public float time = 1;

    private void Awake()
    {
        Destroy(gameObject, time);
    }

}