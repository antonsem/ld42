using UnityEngine;

public class Item : MonoBehaviour
{
    public int id = -1;
    [SerializeField]
    private AudioClip clip;

    private void OnTriggerEnter(Collider other)
    {
        Player p;
        if (other.transform.SetComponent(out p))
            PickUp();
    }

    public void PickUp()
    {
        Debug.Log("Picked up " + name);
        Inventory.Instance.Add(this);
        Events.Instance.itemPickedUp.Invoke();
        gameObject.SetActive(false);
        AudioSource.PlayClipAtPoint(clip, transform.position, 0.25f);
    }
}