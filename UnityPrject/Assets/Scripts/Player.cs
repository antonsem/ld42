using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour, IBreakable
{
    [SerializeField]
    private Gun gun;
    public float fullHealth = 10;
    public float health = 10;

    [Space(10), Header("Abilities")]
    public bool gunActive = true;
    public bool healthActive = true;
    public bool cellCountActive = true;
    public bool sentinelsCountActive = true;
    public bool spaceCountActive = true;
    public bool activateRadar = true;

    private Events events;

    private void Awake()
    {
        events = Events.Instance;
    }

    private void Start()
    {
        if (!gunActive)
            ActivateGun(false);

        events.activateGun.AddListener(ActivateGun);

        events.activateCellsCount.Invoke(cellCountActive);
        events.activateHealth.Invoke(healthActive);
        events.activateSentinels.Invoke(sentinelsCountActive);
        events.activateSpaceCount.Invoke(spaceCountActive);
        events.activateRadar.Invoke(activateRadar);
    }

    public void Hit(float damage)
    {
        health -= damage;
        Events.Instance.playerHit.Invoke();
        if (health <= 0)
            Events.Instance.playerDied.Invoke();
    }

    private void ActivateGun(bool state)
    {
        if (!state)
        {
            gun.gameObject.SetActive(false);
            gun.transform.localPosition = new Vector3(0, -0.3f, 0);
        }
        else
            StartCoroutine(ActivateGun());

        gunActive = state;
    }

    private IEnumerator ActivateGun()
    {
        gun.gameObject.SetActive(true);
        while(Vector3.SqrMagnitude(gun.transform.localPosition) > 0.01f)
        {
            gun.transform.localPosition = Vector3.Lerp(gun.transform.localPosition, Vector3.zero, Time.deltaTime * 2);
            yield return null;
        }
    }

    private void Update()
    {
        if(Input.GetAxis("Fire1") > 0.75f && gunActive)
            gun.Shoot();
    }
}