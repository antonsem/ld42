using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    public float lifeTime = 2;
    public float speed = 1;
    public float damage = 1;
    [SerializeField]
    private GameObject explosion;
    [SerializeField]
    private AudioClip explosionSound;

    private Rigidbody rigid;
    private bool collided = false;

    private void Start()
    {

        if (transform.SetComponent(out rigid))
            rigid.velocity = transform.forward * speed;
        else
        {
            Debug.LogError("There is no RigidBody component on the bullet " + name);
            Destroy(gameObject);
            return;
        }
        Destroy(gameObject, lifeTime);
    }

    private void OnDestroy()
    {
        if(!collided)
        {
            GameObject obj = Instantiate(explosion, transform.position, transform.rotation);
            obj.transform.forward = Vector3.up;
        }

        AudioSource.PlayClipAtPoint(explosionSound, transform.position);
    }

    private void OnCollisionEnter(Collision collision)
    {
        IBreakable t;
        if (collision.transform.SetComponent(out t))
            t.Hit(damage);

        GameObject obj = Instantiate(explosion, transform.position, transform.rotation);
        if (collision.contacts != null && collision.contacts.Length > 0)
            obj.transform.forward = collision.contacts[0].normal;
        collided = true;
        Destroy(gameObject);
    }
}