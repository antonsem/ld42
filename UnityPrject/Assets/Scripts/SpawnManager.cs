using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public int createPerTileDestroyed = 3;
    public int createPerEnemyHit = 4;

    private Player player;
    private Enemy[] enemies;

    private bool updateEnemies = true;

    private void Awake()
    {
        Events.Instance.tileDestroyed.AddListener(() => TileDestroyed(createPerTileDestroyed));
        Events.Instance.enemyHit.AddListener(() => TileDestroyed(createPerEnemyHit));
        Utils.SetObject(out player, true);
    }

    private void TileDestroyed(int replaceCount)
    {
        List<Vector2Int> availablePos = LevelGenerator.Instance.GetAcceptablePositions();
        RemoveEntityPos(availablePos, player.transform.position);
        for (int i = 0; i < enemies.Length; i++)
            RemoveEntityPos(availablePos, enemies[i].transform.position);
        if (availablePos.Count < replaceCount)
        {
            Events.Instance.playerDied.Invoke();
            return;
        }
        foreach (Vector2Int pos in availablePos.PickRandom(replaceCount))
            LevelGenerator.Instance.CreateTile(pos);
    }

    private void RemoveEntityPos(List<Vector2Int> list, Vector3 pos)
    {
        Vector2Int mPos = new Vector2Int(Mathf.FloorToInt(pos.x) / 2, Mathf.FloorToInt(pos.z) / 2);
        if (list.Contains(mPos)) list.Remove(mPos);
        if (list.Contains(mPos + new Vector2Int(1, 0))) list.Remove(mPos + new Vector2Int(1, 0));
        if (list.Contains(mPos + new Vector2Int(1, 1))) list.Remove(mPos + new Vector2Int(1, 1));
        if (list.Contains(mPos + new Vector2Int(0, 1))) list.Remove(mPos + new Vector2Int(0, 1));
    }

    private void LateUpdate()
    {
        if (updateEnemies)
            enemies = FindObjectsOfType<Enemy>();
    }
}