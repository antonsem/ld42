using UnityEngine;

public class Message : MonoBehaviour
{
    private bool sent = false;
    [TextArea(20, 50)]
    public string message = "...";
    public float messagTime = 5;
    [SerializeField]
    private AudioClip clip;

    private void OnTriggerEnter(Collider other)
    {
        Player p;
        if (!sent && other.transform.SetComponent(out p))
        {
            AudioSource.PlayClipAtPoint(clip, transform.position, 0.5f);
            sent = true;
            Events.Instance.sendMessage.Invoke(message, messagTime);
            gameObject.SetActive(false);
        }
    }
}