using System.Linq;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
#endif

public static class Utils
{
    public static bool SetComponent<T>(this Transform t, out T component)
    {
        component = t.GetComponent<T>();
        return component != null;
    }

    public static bool SetObject<T>(out T obj, bool requreWarning = false) where T : Component
    {
        obj = Object.FindObjectOfType<T>();
        if (requreWarning && !obj)
            Debug.LogWarning("Couldn't find object of type " + typeof(T));

        return obj != null;
    }

    // The Random object this method uses.
    private static System.Random Rand = null;

    // Return num_items random values.
    public static List<T> PickRandom<T>(
        this List<T> values, int num_values)
    {
        // Create the Random object if it doesn't exist.
        if (Rand == null) Rand = new System.Random();

        // Don't exceed the array's length.
        if (num_values >= values.Count)
            num_values = values.Count - 1;

        // Make an array of indexes 0 through values.Length - 1.
        int[] indexes =
            Enumerable.Range(0, values.Count).ToArray();

        // Build the return list.
        List<T> results = new List<T>();

        // Randomize the first num_values indexes.
        for (int i = 0; i < num_values; i++)
        {
            // Pick a random entry between i and values.Length - 1.
            int j = Rand.Next(i, values.Count);

            // Swap the values.
            int temp = indexes[i];
            indexes[i] = indexes[j];
            indexes[j] = temp;

            // Save the ith value.
            results.Add(values[indexes[i]]);
        }

        // Return the selected items.
        return results;
    }

    public static void Shuffle<T>(this System.Random rng, T[] array)
    {
        int n = array.Length;
        while (n > 1)
        {
            int k = rng.Next(n--);
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }

    #region Editor Tools
#if UNITY_EDITOR
    [MenuItem("Tools/Clear Console %q")] // CTRL + Q
    private static void ClearConsole()
    {
        var assembly = Assembly.GetAssembly(typeof(SceneView));
        var type = assembly.GetType("UnityEditor.LogEntries");
        var method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }
#endif
    #endregion
}