using System.Collections.Generic;
using UnityEngine;

public class Inventory : Singleton<Inventory>
{
    private List<Item> items = new List<Item>();
    public List<Item> Items
    {
        get
        {
            return items;
        }
        private set
        {
            items = value;
        }
    }

    public void Add(Item item, bool removeDublicate = false)
    {
        if (Items.Contains(item))
        {
            Debug.LogWarning("Item " + item.name + " is already in inventory!");
            if (!removeDublicate)
                Items.Add(item);
        }
        else
            Items.Add(item);
    }

    public void Clear()
    {
        Items = new List<Item>();
    }
}