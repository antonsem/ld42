using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour, IBreakable
{
    private NavMeshAgent agent;
    private Player player;

    [SerializeField]
    private float updateTime = 1;
    private float timer = 0;
    [SerializeField]
    private float stunTime = 5;
    private float stunTimer;
    [SerializeField]
    private float damageOutput = 1;

    private float minDist = 0;

    public void Hit(float damage)
    {
        stunTimer = stunTime;
        Events.Instance.enemyHit.Invoke();
    }

    private void Awake()
    {
        transform.SetComponent(out agent);
        Utils.SetObject(out player);
        minDist = agent.stoppingDistance * agent.stoppingDistance;
    }

    private void Start()
    {
        Events.Instance.enemyIsBorn.Invoke();
    }

    private void Update()
    {
        if (timer <= 0)
        {
            agent.SetDestination(player.transform.position);
            timer = updateTime;
        }
        else
            timer -= Time.deltaTime;

        if (stunTimer > 0)
        {
            if (!agent.isStopped)
                agent.isStopped = true;
            stunTimer -= Time.deltaTime;
        }
        else if (agent.isStopped)
            agent.isStopped = false;

        if (Vector3.SqrMagnitude(transform.position - player.transform.position) <= minDist + 0.5f)
            player.Hit(damageOutput * Time.deltaTime);
    }
}
