using UnityEngine;

public class Tile : MonoBehaviour, IBreakable
{
    public float health = 3;
    public GameObject loot;
    [SerializeField]
    private AudioSource source;

    private void Start()
    {
        source.enabled = loot;
        Events.Instance.tileCreated.Invoke();
    }

    public void Hit(float damage = 1)
    {
        health -= damage;
        if (health <= 0)
            Die();
    }

    protected virtual void Die()
    {
        LevelGenerator.Instance.DestroyTile(gameObject, loot ? Instantiate(loot, transform.position, transform.rotation, transform.parent) : null);
        Events.Instance.tileDestroyed.Invoke();
    }
}