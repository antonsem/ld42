public interface IBreakable
{
    void Hit(float damage);
}