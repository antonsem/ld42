using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField]
    private Button newGame;
    [SerializeField]
    private Button cont;
    [SerializeField]
    private Button credits;
    [SerializeField]
    private Button quit;

    private LevelChanger levelChanger;

    private void Awake()
    {
        newGame.onClick.AddListener(OnNewGame);
        cont.onClick.AddListener(OnContinue);
        credits.onClick.AddListener(OnCredits);
        quit.onClick.AddListener(OnQuit);
        levelChanger = LevelChanger.Instance;
        cont.interactable = levelChanger.currentLevel > 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void OnNewGame()
    {
        levelChanger.StartNewGame();
    }

    private void OnContinue()
    {
        levelChanger.GetCurrentLevel();
        levelChanger.ReloadCurrentLevel();
    }

    private void OnCredits()
    {
        levelChanger.CreditsScene();
    }

    private void OnQuit()
    {
        Application.Quit();
    }
}