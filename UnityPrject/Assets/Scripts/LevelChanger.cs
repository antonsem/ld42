using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : Singleton<LevelChanger>
{
    public int currentLevel = 0;

    public string mainMenu = "MainMenu";
    public string endGame = "EndGame";
    public string gameOver = "GameOver";
    public string end = "Done";
    public string credits = "Credits";
    public string[] scenes = new string[]
    {
        "Level_1",
        "Level_2",
        "Level_3",
        "Level_4",
        "Level_5",
        "Level_6",
        "Level_7",
        "Level_8",
        "Level_9",
        "Level_10"

    };

    public bool done = false;

    private void Awake()
    {
        GetCurrentLevel();
        DontDestroyOnLoad(gameObject);
    }

    private void OnDestroy()
    {
        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
    }

    public void GetCurrentLevel()
    {
        if (PlayerPrefs.HasKey("CurrentLevel"))
            currentLevel = PlayerPrefs.GetInt("CurrentLevel");
    }

    public void SetCurrentLevel()
    {
        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
    }

    public void StartNewGame()
    {
        done = false;
        currentLevel = 0;
        SceneManager.LoadScene(scenes[currentLevel]);
    }

    public void GameOverScene()
    {
        SceneManager.LoadScene(gameOver);
    }

    public void MainMenuScene()
    {
        SceneManager.LoadScene(mainMenu);
    }

    public void CreditsScene()
    {
        SceneManager.LoadScene(credits);
    }

    public void ReloadCurrentLevel()
    {
        SceneManager.LoadScene(scenes[currentLevel]);
    }

    public void ProceedToNextScene()
    {
        if (++currentLevel >= scenes.Length)
        {
            done = true;
            SceneManager.LoadScene(end);
        }
        else
        {
            SceneManager.LoadScene(scenes[currentLevel]);
            SetCurrentLevel();
        }
    }
}